var request = require('superagent');
var assert = require('assert');

var URL = 'https://www.revapm.com/';

describe('Testing an HTTP Response for Home page', function() {

    it('should have a status code 200', function(done) {
        this.timeout(9000);

        request
            .get(URL)
            .end(function(err, response) {
                assert.equal(response.status, 200);
                done();
            });
    });

    it('should have a correct content-type header', function(done) {
        this.timeout(9000);

        request
            .get(URL)
            .end(function(err, response) {
                assert.equal(response.header['content-type'], 'text/html; charset=UTF-8');
                done();
            });
    });
});

describe('Testing Contact Us form request', function() {

    it('should have a status code 200', function(done) {
        this.timeout(9000);

        request
            .post(URL + '/take-action')
            .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
            .send({_wpcf7: '24'})
            .send({_wpcf7_is_ajax_call: '1'})
            .send({_wpcf7_locale: 'en_US'})
            .send({_wpcf7_unit_tag: 'wpcf7-f24-p13-o1'})
            .send({_wpcf7_version: '4.3'})
            .send({_wpnonce: '80813cc94f'})
            .send({'your-email': 'test@gmail.com'})
            .send({'your-message': 'test'})
            .send({'your-name': 'test'})
            .send({'your-subject': 'test'})
            .end(function(err, response) {
                assert.equal(response.status, 200);
                done();
            })
    });

    it('should have correct response value', function(done) {
        this.timeout(9000);

        request
            .post(URL + '/take-action')
            .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
            .send({_wpcf7: '24'})
            .send({_wpcf7_is_ajax_call: '1'})
            .send({_wpcf7_locale: 'en_US'})
            .send({_wpcf7_unit_tag: 'wpcf7-f24-p13-o1'})
            .send({_wpcf7_version: '4.3'})
            .send({_wpnonce: '80813cc94f'})
            .send({'your-email': 'test@gmail.com'})
            .send({'your-message': 'test'})
            .send({'your-name': 'test'})
            .send({'your-subject': 'test'})
            .end(function(err, response) {
                assert.equal(response.text, '<textarea>{"mailSent":true,"into":"#wpcf7-f24-p13-o1","captcha":null,"message":"Your message was sent successfully. Thanks."}</textarea>');
                done();
            })
    });
	
	it('should have a correct content-type header', function(done) {
        this.timeout(9000);

        request
            .post(URL + '/take-action')
            .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
            .send({_wpcf7: '24'})
            .send({_wpcf7_is_ajax_call: '1'})
            .send({_wpcf7_locale: 'en_US'})
            .send({_wpcf7_unit_tag: 'wpcf7-f24-p13-o1'})
            .send({_wpcf7_version: '4.3'})
            .send({_wpnonce: '80813cc94f'})
            .send({'your-email': 'test@gmail.com'})
            .send({'your-message': 'test'})
            .send({'your-name': 'test'})
            .send({'your-subject': 'test'})
            .end(function(err, response) {
                assert.equal(response.header['content-type'], 'text/html; charset=UTF-8');
                done();
            })
    });

});